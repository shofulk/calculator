﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

namespace CalcTest
{
    [TestFixture]
    public class CalculatorTest
    {
        Calc.CalculatorEnger calculatorEnger = new Calc.CalculatorEnger();
        Calc.Calculator calc = new Calc.Calculator();

        [Test]
        public void Test1()
        {
            Assert.AreEqual(calc.addition(2, 5), 7);
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual(calc.subtration(5, 2), 3);
        }

        [Test]
        public void Test3()
        {
            Assert.AreEqual(calc.multiplication(2, 5), 10);
        }

        [Test]
        public void Test4()
        {
            Assert.AreEqual(calc.division(10, 2), 5);
        }

        [Test]
        public void Test5()
        {
            Assert.AreEqual(calculatorEnger.radical(4), 2);
        }

        [Test]
        public void Test6()
        {
            Assert.AreEqual(calculatorEnger.square(2), 4);
        }

        [Test]
        public void Test7()
        {
            Assert.AreEqual(calculatorEnger.sin(0), 0);
        }

        [Test]
        public void Test8()
        {
            Assert.AreEqual(calculatorEnger.cos(0), 1);
        }

        [Test]
        public void Test9()
        {
            Assert.AreEqual(calculatorEnger.tn(0), 0);
        }

        [Test]
        public void Test10()
        {
            Assert.AreEqual(calculatorEnger.negativeNumber(10), -10);
        }

        [Test]
        public void Test11()
        {
            Assert.AreEqual(calculatorEnger.whole(3.5), 3);
        }
    }
}
