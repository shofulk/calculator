﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc
{
    public class CalculatorEnger : Calculator
    {

        public int square(int a) { return multiplication(a, a); }
        public double radical(double a) { return Math.Sqrt(a); }
        public double exponentiation(double a, double b) { return Math.Pow(a, b); }
        public double sin(double a) { return Math.Sin(a); }
        public double cos(double a) { return Math.Cos(a); }
        public double tn(double a) { return Math.Tan(a); }
        public double tng(double a) { return Math.Tanh(a); }
        public double negativeNumber(double a) { return a = -a; }
        public double whole(double a) { return Math.Truncate(a); }

    }
}
