﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc
{
    public class Calculator
    {
        public int addition(int a, int b) { return a + b; }
        public int subtration(int a, int b) { return a - b; }
        public int multiplication(int a, int b) { return a * b; }
        public double division(int a, int b) { return a / b; }
        public double radical(double a) { return Math.Sqrt(a); }
        public int square(int a) { return a * a; }
    }
}
